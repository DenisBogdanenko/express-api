import express from 'express'
import mongoose from 'mongoose'
import router from "./router.js";

const PORT = 5000;
const DB_URL = `mongodb+srv://denis:1111@cluster0.bme0h.mongodb.net/express-api?retryWrites=true&w=majority`

const app = express()

app.use(express.json())
app.use("/api", router)

const start = async () => {
    try {
        await mongoose.connect(DB_URL, {
            useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true
        });

        app.listen(PORT, () => console.log("Server working"))
    } catch (e) {
        console.log(e)
    }
}

start()
